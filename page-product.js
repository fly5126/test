"use strict";
let button = document.getElementById("buttonReview");
let mark = document.querySelector("#inputMark");
let inputName = document.querySelector("#inputName");
let textArea = document.querySelector(".review-textarea");
let cart = {};
let cartCount = 0;
if (localStorage.getItem("cart")) {
    cart = (JSON.parse(localStorage.cart));
    for (let key in cart) {
        cartCount += +(cart[key]);
    }
    if (cartCount > 0) {
        document.querySelector(".cart-count").innerHTML = +cartCount;
        document.querySelector(".cart-count").style.display = "block";
        document.querySelector(".button_cart").innerHTML = "Товар уже в корзине";
    }
}
let cartBtn = document.querySelector(".button_cart");
cartBtn.addEventListener("click", () => {
    cartCount = 0;
    if (cart["Код товара1"] == 1) {
        document.querySelector(".cart-count").innerHTML = "";
        document.querySelector(".cart-count").style.display = "none";
        document.querySelector(".button_cart").innerHTML = "Добавить в корзину";
        delete cart["Код товара1"];
        localStorage.cart = JSON.stringify(cart);
        for (let key in cart) {
            cartCount += +(cart[key]);
        }
    } else {
        cart["Код товара1"] = 1;
        for (let key in cart) {
            cartCount += +(cart[key]);
        }
        document.querySelector(".cart-count").innerHTML = cartCount;
        document.querySelector(".cart-count").style.display = "block";
        document.querySelector(".button_cart").innerHTML = "Товар уже в корзине";
        localStorage.cart = JSON.stringify(cart);
    }
})
if (localStorage.getItem("mark")) {
    mark.value = localStorage.getItem("mark");
}
if (localStorage.getItem("name")) {
    inputName.value = localStorage.getItem("name");
}
if (localStorage.getItem("textReview")) {
    textArea.value = localStorage.getItem("textReview");
}
mark.addEventListener("input", () => {
    localStorage.setItem("mark", mark.value);
})
inputName.addEventListener("input", () => {
    localStorage.setItem("name", inputName.value);
})
textArea.addEventListener("input", () => {
    localStorage.setItem("textReview", textArea.value);
})
button.addEventListener("click", () => {
    document.querySelector(".error").style.display = "none";
    document.querySelector(".errorMark").style.display = "none";
    if ((!mark.value) || (!Number(mark.value)) ||
        (mark.value < 1) || (mark.value > 5)) {
        document.querySelector(".errorMark").style.display = "block";
    } else if (!inputName.value) {
        document.querySelector(".error").style.display = "block";
        document.querySelector(".error").innerHTML = "Вы забыли указать имя!";
        document.querySelector(".errorMark").style.display = "none";
    } else if ((inputName.value.trim().length < 2) && (inputName.value)) {
        document.querySelector(".error").style.display = "block";
        document.querySelector(".error").innerHTML = "Имя не может быть короче двух символов!";
        document.querySelector(".errorMark").style.display = "none";
    } else {
        localStorage.removeItem("mark");
        localStorage.removeItem("name");
        localStorage.removeItem("textReview");
        mark.value = "";
        inputName.value = "";
        textArea.value = "";
        console.log("Ваш отзыв успешно сохранен");
    }
})
mark.addEventListener("click", (event) => {
    document.querySelector(".errorMark").style.display = "none";
})
inputName.addEventListener("click", (event) => {
    document.querySelector(".error").style.display = "none";
})