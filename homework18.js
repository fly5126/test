"use strict";
let a = '100px';
let b = '323px';
let result = parseInt(a)+parseInt(b);// Решение должно быть написано тутconsole.log(result);
console.log(result); // Выводим в консоль, должнополучится 423
console.log(Math.max(10, -45, 102, 36, 12, 0, -1) );

let c = 123.3399;// Округлить до 123
// Решение
console.log(Math.round(c));
let d = 0.111;// Округлить до 1
console.log(Math.ceil(d));
let e = 45.333333;// Округлить до 45.3
console.log(e.toFixed(1));
let f = 3;// Возвести в степень 5 (должно быть 243)
console.log(f**5);
let g = 400000000000000;// Записать в сокращенномвиде
console.log('4e14');
let h = '1' == 1; //Поправить условие, чтобы результат был true 
//(значения изменять нельзя, только оператор)
console.log(h);