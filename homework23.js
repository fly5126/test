"use strict";
//Упражнение 1

let promise = new Promise(function (resolve, reject) {
    let n = prompt("Введите число")
    if (!isNaN(parseFloat(n)) && (n > 0)) {
        resolve(n);
    }
    reject("error");
});
promise
    .then(
        n => {
            let intervalId = setInterval(() => {
                n = n - 1;
                console.log("Осталось " + n);
                if (n === 0) {
                    console.log("Время вышло");
                    clearInterval(intervalId);
                }
            }, 1000);
        },
        error => { console.log("Что-то пошло нетак") }
    );
//Упражнение 2
/**
 *  выводит данные пользователей 
 * @param {*} obj  JSON
  */
function GetInfo(obj) {
    console.log("Получили пользователей: " + obj.data.length);
    obj.data.forEach(element => {
        console.log("- " + element.first_name + " " + element.last_name + " (" + element.email + ")")
    });
    

};


const start = new Date().getTime();
let promise2 = fetch("https://reqres.in/api/users");
const end = new Date().getTime();
console.log(`Время запроса: ${end - start}ms`);
promise2
    .then(response => response.json())
    .then(result => GetInfo(result))
    .catch(error => console.log({ error }));




