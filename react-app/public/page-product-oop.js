"use strict";
class Form {
    send(msg) {
        alert(msg);
    }
}
class AddReviewForm extends Form {
    constructor(
        formSelector,
        nameSelector,
        markSelector,
        textSelector,
        errorSelector,
        errorMarkSelector
    ) {
        super();
        this.form = document.querySelector(formSelector);
        this.name = document.querySelector(nameSelector);
        this.mark = document.querySelector(markSelector);
        this.text = document.querySelector(textSelector);
        this.error = document.querySelector(errorSelector);
        this.errorMark = document.querySelector(errorMarkSelector);
        this.form.addEventListener('submit', this.validate);
        this.name.addEventListener('focus', this.clearErrors);
        this.mark.addEventListener('focus', this.clearErrors);
        this.name.addEventListener('input', this.setStorage);
        this.mark.addEventListener('input', this.setStorage);
        this.text.addEventListener('input', this.setStorage);
        document.addEventListener('DOMContentLoaded', this.getStorage);
    }
    validate = () => {
        if (!this.name.value) {
            this.error.style.display = "block";
            this.error.innerHTML = "Вы забыли указать имя!";
            return;
        }
        if ((this.name.value.trim().length < 2) && (this.name.value)) {
            this.error.style.display = "block";
            this.error.innerHTML = "Имя не может быть короче двух символов!";
            return;
        }
        if ((!this.mark.value) || (!Number(this.mark.value)) ||
            (this.mark.value < 1) || (this.mark.value > 5)) {
            this.errorMark.style.display = "block";
            return;
        }
        localStorage.clear();
        this.mark.value = "";
        this.name.value = "";
        this.text.value = "";
        this.send("Отзыв сохранен")
    }
    clearErrors = () => {
        this.error.style.display = "none";
        this.errorMark.style.display = "none";
    }
    setStorage = () => {
        localStorage.setItem("mark", this.mark.value);
        localStorage.setItem("name", this.name.value);
        localStorage.setItem("textReview", this.text.value);
    }
    getStorage = () => {
        if (localStorage.getItem("mark")) {
            this.mark.value = localStorage.getItem("mark");
        }
        if (localStorage.getItem("name")) {
            this.name.value = localStorage.getItem("name");
        }
        if (localStorage.getItem("textReview")) {
            this.text.value = localStorage.getItem("textReview");
        }
    }
}

let form1 = new AddReviewForm(
    ".review-form",
    '.surname',
    '.mark',
    '.review-textarea',
    '.error',
    '.errorMark'
);