import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import PageProduct from './components/pageproduct/PageProduct';
import PageIndex from './components/PageIndex/PageIndex';
import PageNotFound from './components/PageNotFound/PageNotFound';
import Header from './components/header/Header';
import Footer from './components/footer/Footer';

function App() {
  return (
    <>
    <Header/>
      <BrowserRouter >
        <Routes >
          <Route path="/" element={<PageIndex />} />
          <Route path="/product" element={<PageProduct />} />
          <Route path="*" element={<PageNotFound />} />
        </Routes>
      </BrowserRouter>
      <Footer/>

    </>
  );
}

export default App;
