import React, { useState } from 'react';
import './Sidebar.css';
import Cart from '../cart/Cart';




function Sidebar() {
    const [Heart, setHeart] = useState("ignore");
    const handleChangeHeart = (e) => {
        setHeart((prevHeart) => { return prevHeart === "ignore" ? "like" : "ignore" })
    }
    return (
        <div className="sidebar">
            <div className="important">
                <div className="cost-box">
                    <div className="cost">
                        <div className="cost__old">
                            <span className="cost_decor">75&nbsp;990&#8381;</span>
                            <div className="sale">
                                -8%
                            </div>
                        </div>

                        <div className="cost__new">67 990&#8381;</div>
                    </div>
                    <div className="heart">{ }
                        <svg
                            onClick={(e) => { handleChangeHeart(e) }}
                            className={`svg-img ${Heart === "like" ? "hidden" : ""}`}
                            width="28"
                            height="22"
                            viewBox="0 0 28 22"
                            xmlns="http://www.w3.org/2000/svg">
                            <path
                                className="heart-img"
                                fillRule="evenodd"
                                clipRule="evenodd"
                                d="M2.78502 2.57269C5.17872 0.274736 9.04661 0.274736 11.4403 2.57269L14.0001 5.03017L16.56 2.57269C18.9537 0.274736 22.8216 0.274736 25.2154 2.57269C27.609 4.87064 27.609 8.5838 25.2154 10.8818L14.0001 21.6483L2.78502 10.8818C0.391321 8.5838 0.391321 4.87064 2.78502 2.57269ZM9.67253 4.26974C8.25515 2.90905 5.97018 2.90905 4.55278 4.26974C3.1354 5.63043 3.1354 7.82401 4.55278 9.18476L14.0001 18.2542L23.4476 9.18476C24.865 7.82401 24.865 5.63043 23.4476 4.26974C22.0302 2.90905 19.7452 2.90905 18.3279 4.26974L14.0001 8.42432L9.67253 4.26974Z"
                                fill="#888888"
                            />
                        </svg>
                        <svg onClick={(e) => { handleChangeHeart(e) }}
                            className={`svg-img ${Heart === "ignore" ? "hidden" : ""}`}
                            xmlns="http://www.w3.org/2000/svg" width="32" height="30" viewBox="0 0 50 50" fill="none">
                            <path fillRule="evenodd" clipRule="evenodd" d="M6.30841 10.9545C10.2979 7.12455 16.7444 7.12455 20.7339 10.9545L25.0002 15.0503L29.2667 10.9545C33.2563 7.12455 39.7027 7.12455 43.6923 10.9545C47.6817 14.7844 47.6817 20.973 43.6923 24.803L25.0002 42.7472L6.30841 24.803C2.31891 20.973 2.31891 14.7844 6.30841 10.9545ZM17.7876 13.7829C15.4253 11.5151 11.617 11.5151 9.25468 13.7829C6.89237 16.0507 6.89237 19.7067 9.25468 21.9746L25.0002 37.0904L40.7461 21.9746C43.1084 19.7067 43.1084 16.0507 40.7461 13.7829C38.3838 11.5151 34.5755 11.5151 32.2132 13.7829L25.0002 20.7072L17.7876 13.7829Z" fill="#F36223" />
                            <path d="M9 13.5L25 29L41 13.5" stroke="#F36223" strokeWidth="13" />
                        </svg>
                    </div>
                </div>
                <div>
                    <div className="date-self"> Самовывоз в четверг, 1 сентября &#8212;<span className="cost__dostavka">
                        бесплатно</span>
                    </div>
                    <div className="date-cura">
                        Курьером в четверг, 1 сентября &#8212;<span className="cost__dostavka"> бесплатно</span>
                    </div>
                </div>
                <Cart product={{ id: 1101, name: "IPhone 13" }} />
            </div>
            <div className="reklama">
                <div>Реклама</div>
                <div className="reklama__list">
                    <iframe title="myFrame" className="reklama__frame" src={""}></iframe>

                    <iframe title="myFrame" className="reklama__frame" src={''} ></iframe>

                </div>

            </div>
        </div>
    )
}

export default Sidebar