import styled from 'styled-components'
const UserReview = styled.div
    `
        display: flex;
        padding: 0px;
        gap: 30px;`;
const UserReviewText = styled.div
    `
        display: flex;
        flex-direction: column;
        padding: 0px;
        gap: 20px;
        width: 710px;`;
const UserPhoto = styled.img
    `
        width: 140px;
        height: 140px;
        border-radius: 100px;
        max-width: 100%;`;
const UserReviewTitle = styled.div
    `
        font-weight: 700;
        font-size: 20px;
        line-height: 24px;
        display: flex;
        flex-direction: column;
        padding: 0px;
        gap: 10px;
        width: 150px;
        height: 64px;`;
const UserReviewRait = styled.div
    `
    
        display: flex;
        padding: 0px;
    `;


export { UserReview, UserReviewText, UserPhoto, UserReviewTitle, UserReviewRait };