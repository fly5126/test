import React from "react";
import userPhoto from "./avatar-1a86fa6e9c12f3a5404dd70699ef5041.jpg";
import userPhoto2 from "./imageUser.jpeg";
import { UserReview, UserReviewText, UserPhoto, UserReviewTitle, UserReviewRait } from './StyledComponent'
function Reviews() {
    const numbers = [
        {
            id: 0,
            photoPath: userPhoto,
            name: 'Марк Г.',
            rait: 5,
            usage: 'менее месяца',
            plus:
                'это мой первый айфон после после огромного количества телефонов на андроиде. всё плавно, чётко и красиво. довольно шустрое устройство. камера весьма неплохая, ширик тоже на высоте.',
            minus:
                'к самому устройству мало имеет отношение, но перенос данных с андроида - адская вещь) а если нужно переносить фото с компа, то это только через itunes, который урезает исходное качество фотографий',
        },
        {
            id: 1,
            photoPath: userPhoto2,
            name: 'Василий Г.',
            rait: 4,
            usage: 'менее месяца',
            plus:
                'OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго',
            minus:
                'Плохая ремонтопригодность',
        }
    ];
    return (
        <>
            {numbers.map(
                function (number) {
                    return (<div key={number.id}>
                        <UserReview>
                            <div>
                                <UserPhoto src={number.photoPath} alt="userImage" />
                            </div>
                            <UserReviewText>
                                <UserReviewTitle>
                                    <span>
                                        <b>{number.name}</b>
                                    </span>
                                    <UserReviewRait>
                                        <div className="star-bord">
                                        </div>
                                        <div className="star-bord">
                                        </div>
                                        <div className="star-bord">
                                        </div>
                                        <div className="star-bord">
                                        </div>
                                        <div className="star-bord">
                                        </div>
                                    </UserReviewRait>
                                </UserReviewTitle>
                                <div>
                                    <p className="user-review-info"><b>Опыт использования:</b> {number.usage}</p>
                                    <p className="user-review-info"><b>Достоинства:<br /></b> {number.plus}
                                    </p>
                                    <p className="user-review-info"><b>Недостатки:<br /></b> {number.minus}
                                    </p>
                                </div>
                            </UserReviewText>
                        </UserReview>
                        <hr className="line" />
                    </div>
                    )
                }
            )
            }
        </>
    )
}
export default Reviews; 