import React from 'react';
import styles from './Header.module.css';
import logoImage from './favicon.svg';
import cn from 'classnames';
import  CartCount  from '../cartCount/CartCount';



function Header() {
    return (
        <div className={styles.header}>
            <a className={cn(styles.logolink)} href='/'>
                <div className={styles.logoBox}>
                    <div className={styles.logoImg}>
                        <img className={styles.logotip} src={logoImage} alt="Логотип" />
                        <div>
                            <span className={styles.logo}>Мой</span><span>Маркет</span>
                        </div>
                    </div>
                </div>
            </a>
            <CartCount />
        </div>
    )
}
export default Header
