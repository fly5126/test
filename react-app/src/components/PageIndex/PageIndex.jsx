import React from 'react';
import './PageIndex.css'

function PageIndex() {
    return (
        <div className='index-container'>
            Здесь должно быть содержимое главной страницы.
            Но в рамках курса главная страница  используется лишь
            в демонстрационных целях <br/>
           <p><a className='link' href='/product'> Перейти на страницу товара</a></p>
        </div>
    )
}

export default PageIndex