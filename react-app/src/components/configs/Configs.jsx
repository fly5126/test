
import ConfigButton from "./ConfigButton";
import React, { useState } from 'react'
function Configs() {
    const numbers = [{ memory: '128 Гб' }, { memory: '256 Гб' }, { memory: '512 Гб' }];
    const [activedConfig, setActivedConfig] = useState('128 Гб');
    const handleClick = (e, memory) => {
        setActivedConfig(memory);
        const textMemory = `Конфигурация памяти: ${memory}`;
        document.querySelector(".change-memory").innerHTML = textMemory;
    }
    return (
        <div className="Buttons">
            {numbers.map(
                function (number) {
                    return <div className="ButtonContainer" key={number.memory}>
                        <ConfigButton
                            memory={`${number.memory}`}
                            onClick={(e) => handleClick(e, number.memory)}
                            actived={number.memory === activedConfig}
                        />
                    </div>
                })}
        </div>
    );

}
export default Configs;