
import './ConfigButton.css'
function ConfigButton(props) {
    const { memory, actived, ...restProps } = props;
    const className = `btn confbtn ${actived ? "btn_actived" : ""}`;
    return (
        <button {...restProps} className={className}>
            {memory}
        </button>
    )
}
export default ConfigButton;