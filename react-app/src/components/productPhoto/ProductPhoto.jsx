import React from 'react';
import Image1 from './image-1.webp'
import Image2 from './image-2.webp'
import Image3 from './image-3.webp'
import Image4 from './image-4.webp'
import Image5 from './image-5.webp'

function ProductPhoto() {
    
  return (
    <div className="img-box">
    <figure className="img-container">
      <img className="foto-tovar" src={Image1} alt="foto tovara" />
    </figure>
    <figure className="img-container">
      <img className="foto-tovar" src={Image2} alt="foto tovara" />
    </figure>
    <figure className="img-container">
      <img className="foto-tovar" src={Image3} alt="foto tovara" />
    </figure>
    <figure className="img-container">
      <img className="foto-tovar" src={Image4} alt="foto tovara" />
    </figure>
    <figure className="img-container">
      <img className="foto-tovar" src={Image5} alt="foto tovara" />
    </figure>
  </div>
  )
}

export default ProductPhoto