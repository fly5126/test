import React from 'react';
import './PageProduct.css';

import Sidebar from '../sidebar/Sidebar';
import Colors from '../colors/Colors';
import Configs from '../configs/Configs'
import Reviews from '../reviews/Reviews'
import ProductPhoto from '../productPhoto/ProductPhoto';
import AddReview from '../addReview/AddReview';




function PageProduct() {
  return (
    <>
      <div id="top" className="body">
    
        <nav className="crumbs">
          <a className="link" href="/#">Электроника</a> {'>'}
          <a className="link" href="/#">Смартфоны и гаджеты</a> {'>'}
          <a className="link" href="/#">Мобильные телефоны</a> {'>'}
          <a className="link" href="/#">Apple</a>
        </nav>
        <main className="main">
          <h1 className="main-title">Смартфон Apple iPhone 13, синий</h1>
         
          <ProductPhoto />
          <div className="haracteristic-box">
            <div className="product-description">
              <section className="product-info">
                <h3 className="change-color h3-index">Цвет товара: Синий</h3>
                <Colors />
              </section>
              <section className="product-info">
                <h3 className="change-memory h3-index">Конфигурация памяти: 128 ГБ</h3>
                <Configs />
              </section>

              <section className="product-info">
                <h3 className="h3-index">Характеристики товара</h3>
                <ul className="product-info__list">
                  <li className="product-info__option"><span>Экран: <b>6.1</b></span></li>
                  <li className="product-info__option"><span>Встроенная память: <b>128 ГБ</b></span></li>
                  <li className="product-info__option"><span>Операционная система: <b>iOS 15</b></span></li>
                  <li className="product-info__option"><span>Беспроводные интерфейс: <b>NFC, Bluetooth,
                    Wi-Fi</b></span></li>
                  <li className="product-info__option"><span>Процессор: <b><a className="link"
                    href="https://ru.wikipedia.org/wiki/Apple_A15" target="_blank" rel="noreferrer">Apple
                    A15 Bionic</a></b></span></li>
                  <li className="product-info__option"><span>Вес: <b>173 г</b></span></li>
                </ul>
              </section>
              <section className="product-info" >
                <h3 className="h3-index">Описание</h3>
                <p className="product-info__text" >
                  Наша самая совершенная система двух камер.<br />Особый взгляд на прочность дисплея.<br />Чип, с
                  которым
                  всё
                  супербыстро.<br />
                  Аккумулятор держится заметно дольше.<br /><i>iPhone 13 - сильный мира всего.</i>
                </p>
                <p className="product-info__text">
                  Мы разработали совершенно новую схему расположения и развернули объективы на 45 градусов.
                  Благодаря
                  этому
                  внутри
                  корпуса поместилась наша лучшая система двух камер с увеличенной матрицей широкоугольной
                  камеры.
                  Кроме
                  того,
                  мы освободили место для системы оптической стабилизации изображения сдвигом матрицы. И
                  повысили
                  скорость
                  работы
                  матрицы на сверхширокоугольной камере.
                </p>
                <p className="product-info__text">
                  Новая сверхширокоугольная камера видит больше деталей в тёмных областях снимков. Новая
                  широкоугольная
                  камера
                  улавливает
                  на 47% больше света для более качественных фотографий и видео. Новая оптическая стабилизация
                  со
                  сдвигом
                  матрицы обеспечит
                  чёткие кадры даже в неустойчивом положении.
                </p>
                <p className="product-info__text">
                  Режим "Киноэффект" автоматически добавляет великолепные эффекты перемещения фокуса и
                  изменения
                  резкозти.
                  Просто начните запись видео. Режим "Киноэффект" распознаёт, когда нужно перевести фокус на
                  другого
                  человека
                  или объект, который появился в кадре. Теперь ваши видео будут смотреться как настоящее кино.
                </p>
              </section>
              <section className="tablica">
                <h3 className="h3-index">Сравнение моделей</h3>
                <table className="sravnenie">
                  <tbody>
                    <tr className="shapka">
                      <th className="td">Модель</th>
                      <th className="td">Вес</th>
                      <th className="td">Высота</th>
                      <th className="td">Ширина</th>
                      <th className="td">Толщина</th>
                      <th className="td">Чип</th>
                      <th className="td">Объем памяти</th>
                      <th className="td">Беспроводная зарядка</th>
                      <th className="td">Аккумулятор</th>
                    </tr>
                    <tr className="tr">
                      <td className="td">Iphone11</td>
                      <td className="td">194 грамма</td>
                      <td className="td">150.9 мм</td>
                      <td className="td">75.7 мм</td>
                      <td className="td">8.3 мм</td>
                      <td className="td">A13 Bionicchip</td>
                      <td className="td">до 128 Гб</td>
                      <td className="td">Нет</td>
                      <td className="td">До 17 часов</td>
                    </tr>
                    <tr className="tr">
                      <td className="td">Iphone12</td>
                      <td className="td">164 грамма</td>
                      <td className="td">146.7 мм</td>
                      <td className="td">71.5 мм</td>
                      <td className="td">7.4 мм</td>
                      <td className="td">A14 Bionicchip</td>
                      <td className="td">до 256 Гб</td>
                      <td className="td">Да</td>
                      <td className="td">До 19 часов</td>
                    </tr>
                    <tr className="tr">
                      <td className="td">Iphone13</td>
                      <td className="td">174 грамма</td>
                      <td className="td">146.7 мм</td>
                      <td className="td">71.5 мм</td>
                      <td className="td">7.65 мм</td>
                      <td className="td">A15 Bionicchip</td>
                      <td className="td">до 512 Гб</td>
                      <td className="td">Да</td>
                      <td className="td">До 19 часов</td>
                    </tr>
                  </tbody>
                </table>
              </section>
            </div>
            <Sidebar />
          </div>
          <section className="reviews">
            <div className="reviews-zagolovok">
              <div className="reviews-text">
                <h3 className="reviews-title">Отзывы</h3><span className="reviews-count"> 2534</span>
              </div>
            </div>
            <div className="reviews-list">
              <Reviews />
             <AddReview />
            </div>
          </section>
        </main>
      </div>
     
    </>
  )
}
export default PageProduct