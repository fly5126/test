import React, { useState } from 'react';
import './AddReview.css'

function AddReview() {
    const [errorName, setErrorName] = useState('');
    const [errorMark, setErrorMark] = useState('');
    const [inputName, setInputName] = useState(localStorage.getItem("name") ? localStorage.getItem("name") : "");
    const [inputMark, setInputMark] = useState(localStorage.getItem("mark") ? localStorage.getItem("mark") : "");
    const [inputText, setInputText] = useState(localStorage.getItem("text") ? localStorage.getItem("text") : "");
    const handleSubmit = (e, name, mark) => {
        e.preventDefault();
        if (name.trim() === "") {
            setErrorName("Вы забыли указать имя!");
            return;
        }
        else if ((name.trim().length < 2) && (name)) {
            setErrorName("Имя не может быть короче двух символов!");
            return;
        }
        else if ((mark === "") || (!Number(mark)) ||
            ((Number(mark)) < 1) || ((Number(mark)) > 5)) {
            setErrorMark("Оценка должна быть от 1 до 5");
            return;
        }
        else {
            localStorage.removeItem('name');
            localStorage.removeItem('mark');
            localStorage.removeItem('text');
            setInputName("");
            setInputMark("");
            setInputText("");
            setErrorName("");
            setErrorMark("");
            alert("Отзыв успешно сохранен")
        }
    }
    const handleInputName = (e) => {
        const name = e.target.value;
        setInputName(name);
        localStorage.setItem("name", name);
    }
    const handleInputMark = (e) => {
        const mark = e.target.value;
        setInputMark(mark);
        localStorage.setItem("mark", mark);
    }
    const handleInputText = (e) => {
        const text = e.target.value;
        setInputText(text);
        localStorage.setItem("text", text);
    }
    const handleClearError = (e) => {
        setErrorMark('');
        setErrorName('');
    }
    return (
        <form name="form" className="review-form">
            <h3 className="h3-index">Добавить свой отзыв</h3>
            <div className="review-autor">
                <input
                    onInput={(e) => { handleInputName(e) }}
                    onClick={(e) => { handleClearError(e) }}
                    name="form"
                    id="inputName"
                    className='surname'
                    type="text"
                    placeholder="Имя и фамилия"
                    value={inputName}
                />
                <input
                    onInput={(e) => { handleInputMark(e) }}
                    onClick={(e) => { handleClearError(e) }}
                    name="form"
                    id="inputMark"
                    className="mark"
                    type="text"
                    placeholder="Оценка"
                    value={inputMark}
                />
            </div>
            <div className={`error ${((errorName === "") ? "hidden" : "")}`}>{errorName}</div>
            <div className={`errorMark ${((errorMark === "") ? "hidden" : "")}`}>{errorMark}</div>
            <textarea
                value={inputText}
                onInput={(e) => { handleInputText(e) }}
                onClick={(e) => { handleClearError(e) }}
                name="form"
                className="review-textarea"
                placeholder="Текст отзыва"
            >
            </textarea>
            <button
                onClick={(e) => { handleSubmit(e, inputName, inputMark) }}
                type="submit"
                name="form"
                id="buttonReview"
                className="button_review">Отправить отзыв</button>
        </form>
    )
}

export default AddReview