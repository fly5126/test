import { useSelector, useDispatch } from "react-redux";
import { addProduct, removeProduct } from '../../reducers/cart-reducer';
import styles from './Cart.module.css';
import classNames from "classnames";


function Cart(e, product) {
    const products = useSelector((store) => store.cart.products);
    const dispatch = useDispatch();
    
    const hasInCart = products.some(
        (prevProduct) => prevProduct.id === product.id
    );
    
    function handleAddProduct(e) {
        dispatch(addProduct(product));
        
    }
    function handleRemoveProduct(e) {
        dispatch(removeProduct(product));
        
    }
    return (<>
        {hasInCart ? (
            <button
                onClick={(e) => handleRemoveProduct(e, product)}
                className={classNames(styles.buttonCart, styles.inCart)}>Товар уже в корзине
            </button>
        ) : (
            <button
                onClick={(e) => handleAddProduct(e, product)}
                className={styles.buttonCart}>
                <span className={styles.costDostavka}>Добавить в корзину</span>
            </button>
        )}

    </>)
}

export default Cart