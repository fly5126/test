import './Button.css';
function ColorsButton(props) {
    const { colorNumber, actived, ...restProps } = props;
    const imageSrc = "./color-" + colorNumber + ".webp";
    const className = `btn ${actived ? "btn_actived" : ""}`

    return (
        <button {...restProps} className={className}>
            <img
                src={imageSrc}
                alt='ColorPhoneImg'
            />
        </button>
    )
}
export default ColorsButton;