import React from 'react';
import { useState } from 'react';
import ColorsButton from "./ColorsButton.jsx";
import './Colors.css'

function Colors() {
    const numbers = [{ id: 1, color: "Красный" }, { id: 2, color: "Зеленый" }, { id: 3, color: "Розовый" }, { id: 4, color: "Синий" },
    { id: 5, color: "Белый" }, { id: 6, color: "Черный" }];
    const [activedColor, setActivedColor] = useState(4);


    const handleClick = (e, id, color) => {
        setActivedColor(id);
        const textColor = `Цвет: ${color}`;
       
        document.querySelector(".change-color").innerHTML = textColor;
       

    };
    return (
        <div className="Buttons">
            {numbers.map((number) => {
                return <div className="ButtonContainer" key={number.id}>
                    <ColorsButton
                        color={number.color}
                        colorNumber={number.id}
                        actived={number.id === activedColor}
                        onClick={(e) => handleClick(e, number.id, number.color)}
                    />
                </div>
            })}
        </div>
    );
}
export default Colors;