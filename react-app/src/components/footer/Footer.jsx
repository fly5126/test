import React from 'react';
import './Footer.css';
import { useCurrentDate } from '@kundinos/react-hooks'

function Footer() {
    const date = useCurrentDate({ every: 'day' });
    const year = date.getFullYear();

    return (
        <footer className="footer">
            <div className="copyright">
                &#169; ООО "<span className="logo-text">Мой</span>Маркет", 2018-{year}. <React.Fragment><br /></React.Fragment>
                Для уточнения информации звоните по номеру <a className="link" href="tel:+79000000000">+7 900 000 0000</a>, <React.Fragment><br /></React.Fragment>
                а предложения по сотрудничеству отправляйте на почту <a className="link" href="mailto:partner@mymarket.com">partner@mymarket.com</a>
            </div>
            <a className="up" href="#top">Наверх</a>
        </footer>
    )
}

export default Footer