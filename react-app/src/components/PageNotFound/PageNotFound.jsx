import React from 'react';
import './PageNotFound.css'

function PageIndex() {
    return (
        <div className='not-found-container'>
            <h1>404 Страница не найдена</h1>
            <p><a className='link' href='/product'> Перейти на страницу товара</a></p>
        </div>
    )
}

export default PageIndex