import { useSelector } from "react-redux";
import styles from './CartCount.module.css';
import cartImage from './cart.svg';
import React from 'react';
import cn from 'classnames'


function CartCount() {
    const count = useSelector((store) => store.cart.products.length);
    const isProductInCart = count !== 0
    return (
        <div className={styles.cartBox} >
            <div className={styles.cartIcon}>
                <img className={styles.cartImg} src={cartImage} alt="Корзина" />
                {isProductInCart ? (<div className={styles.cartCount}>{count}</div>
                ) : (
                    <div className={cn(styles.cartCount, styles.cartCountNull)}>{count}</div>)}
            </div>
        </div >
    );
}
export default CartCount




