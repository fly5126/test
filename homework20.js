"use strict";
//Упражнение 1

for (let i = 0; i <= 20; i++) {
    if (i % 2 != 0) continue;
    console.log(i);
}

//Упражнение 2
let sum = 0;
let count = 0;
while (true) {
    let value = +prompt("Введите число", "");
    if (!value) {
        alert("Ошибка, вы ввели не число"); break;
    }
    sum += value;
    count++;
    if (count >= 3) { console.log("Сумма: " + sum); break; }
}

//Упражнение 3
let month = 0;
function getNameOfMonth(month) {
    switch (month) {
        case 0:
            return "Январь";
            break;
        case 1:
            return "Февраль";
            break;
        case 2:
            return "Март";
            break;
        case 3:
            return "Апрель";
            break;
        case 4:
            return "Май";
            break;
        case 5:
            return "Июнь";
            break;
        case 6:
            return "Июль";
            break;
        case 7:
            return "Август";
            break;
        case 8:
            return "Сентябрь";
            break;
        case 9:
            return "Октябрь";
            break;
        case 10:
            return "Ноябрь";
            break;
        case 11:
            return "Декабрь";
            break;
    }
}
for (; month <= 11; month++) {
    if (month === 9) continue;
    console.log(getNameOfMonth(month));
}