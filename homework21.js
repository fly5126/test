"use strict";
//Упражнение 1
let obj = {};
/**
 * Проверяет объект на наличие свойств
 * @param {object} obj объект, проверяемый на наличие свойств 
 * @return {boolean} true, если у объекта нет свойств, иначе false
 */
function isEmpty(obj) {

    for (let key in obj) {
        return false;
    }
    return true;
}
console.log(isEmpty(obj));

//Упражнение 3

/**
 * производит повышение зарплаты на определенный процент
 * @param {number} perzent Определенный процент
 * @return {object} salaries объект с новыми зарплатами
 */
function raiseSalary(perzent) {
    for (let key in salaries) {
        salaries[key] = salaries[key] * perzent / 100 + salaries[key];
        salaries[key] = +salaries[key].toFixed(0);
    }
    return salaries;
}
/**
 * Суммирует значения всех зарплат
 * @param {object} salaries 
 * @return {number} sum Сумма зарплат сотрудников.
 */
function summarizesSalary(salaries) {
    let sum = 0;
    for (let key in salaries) {
        sum += salaries[key];
    }
    return sum;
}
let salaries = {
    John: 100000,
    Ann: 160000,
    Pete: 130000,
};
console.log(summarizesSalary(raiseSalary(5)));
