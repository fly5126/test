"use strict";
//Упражнение 1

/**
 * принимает любоймассив и возвращает сумму чисел в этом массиве
 * @param {Array} arr массив для расчета суммы
 * @return {Number} sum сумма чисел в массиве
 */
function getSumm(arr) {
    let sum = 0;
    arr.forEach(function (n) {
        if (typeof n === "number") {
            sum += n;
        }
    })
    return sum;
}

let arr1 = [1, 2, 10, 5];
alert(getSumm(arr1));// 18
let arr2 = ["a", {}, 3, 3, -2];
alert(getSumm(arr2));// 4

//Exercise 3

/**
 * Добавляет id в массив уникальных ids (корзину)
 * @param {Number} id id товара
 * @return {Array} cart массив id товаров в корзине
 */
function addToCart(id) {
    let set = new Set(cart);
    set.add(id);
    return (cart = Array.from(set));
}

/**
 * удаляет id из массива (корзины)
 * @param {Number} id id удаляемого товара
 * @return {Array} cart массив id товаров в корзине
 */
function removeFromCart(id) {
    cart = cart.filter((n) => n !== id);
}

// В корзине один товар
let cart = [4884];
// Добавили товар
addToCart(3456);
console.log(cart);// [4884, 3456]
// Повторно добавили товар
addToCart(3456);
console.log(cart);// [4884, 3456]
// Удалили товар
removeFromCart(4884);
console.log(cart);// [3456]